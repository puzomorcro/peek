import os

const (
  Endl = '\n'
  AlphaSmall = 'abcdefghijklmnopqrstuvwxyz'
  AlphaCaps = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
  Num = '1234567890'
  Alpha = AlphaSmall + AlphaCaps
  AlphaNum = Alpha + Num
)

// this language is bad and this function proves it
fn value_or_default(text string, optional ?string) string {
  text_new := optional or {
    return text
  }
  return text_new
}

struct OptionalResult {
  value string
  ok bool
}
fn get_result(optional ?string) OptionalResult {
  text_new := optional or {
    return OptionalResult{ '', false }
  }
  return OptionalResult{ text_new, true }
}

fn any(arr []bool) bool {
  for val in arr {
    if val {
      return true
    }
  }
  return false
}

fn all(arr []bool) bool {
  for val in arr {
    if !val {
      return false
    }
  }
  return true
}

fn print_usage(filename string) {
  println('Usage:')
  println('\t$filename FILE_NAME LINE_NUMBER')
}

fn match_whitespace(text string) string {
  mut i := 0
  for i < text.len {
    c := text[i]
    if c == ` ` || c == `\t` || c == `\n` || c == `\r` {
      i++
      continue
    }
    break
  }
  return text.substr(i, text.len)
}

fn is_end_of_line(text string) bool {
  mut i := 0
  for i < text.len {
    c := text[i]
    if c == ` ` || c == `\t` {
      i++
      continue
    }
    break
  }
  return i == text.len || text[i] == `\n` || text[i] == `\r`
}

fn match_keyword(text string, keyword string) ?string {
  if text.len < keyword.len {
    return error('')
  }

  if text.substr(0, keyword.len) == keyword {
    return text.substr(keyword.len + 1, text.len)
  }

  return error('')
}

fn match_keyword_chain(text string, keywords []string) string {
  mut text_new := text

  mut tested := [false].repeat(keywords.len)

  for !all(tested) {
    mut matched := false
    for i, keyword in keywords {
      if tested[i] {
        continue
      }
      matched_text_new := match_keyword(text_new, keyword) or {
        continue
      }
      text_new = match_whitespace(matched_text_new)
      tested[i] = true
      matched = true
      break
    }
    if !matched {
      break
    }
  }

  if text_new.len == text.len {
    return text
  }

  return text_new
}

fn match_char(text string, chars string) ?string {
  if text.len == 0 {
    return error('')
  }

  for c in chars {
    if c == text[0] {
      return text.substr(1, text.len)
    }
  }
  return error('')
}

fn match_api(text string) ?string {
  mut text_new := text

  {
    matched := match_char(text_new, AlphaCaps) or { return error('') }
    text_new = matched
  }

  for {
    matched := match_char(text_new, AlphaCaps) or { break }
    text_new = matched
  }

  {
    matched := match_char(text_new, '_') or { return error('') }
    text_new = matched
  }

  {
    matched := match_keyword(text_new, 'API') or { return error('') }
    text_new = matched
  }

  return text_new
}

fn match_identifier(text string) ?string {
  mut text_new := match_char(text, '_~' + Alpha) or {
    return error('')
  }

  for {
    matched := match_char(text_new, '_' + AlphaNum) or {
      break
    }
    text_new = matched
  }

  return text_new
}

fn match_type(text string) ?string {
  mut text_new := text
  has_parens := get_result(match_char(text_new, '('))
  if has_parens.ok {
    text_new = has_parens.value
  }

  text_new = value_or_default(text_new, match_keyword(text_new, 'const'))
  text_new = match_whitespace(text_new)

  matched_identifier := match_identifier(text_new) or { return error('') }
  text_new = match_whitespace(matched_identifier)

  text_new = value_or_default(text_new, match_char(text_new, '*&'))
  text_new = match_whitespace(text_new)

  text_new = value_or_default(text_new, match_char(text_new, '*&'))
  text_new = match_whitespace(text_new)

  if has_parens.ok {
    matched := match_char(text_new, ')') or {
      return error('')
    }
    text_new = matched
  }

  return text_new
}

fn match_until(text string, c string) ?string {
  mut text_new := text
  mut i := 0
  for i < text_new.len {
    if text_new[i] == c[0] {
      break
    }
    i++
  }
  text_new = text_new.substr(i, text_new.len)
  return match_char(text_new, c)
}

fn match_structor(text string) ?string {
  t1 := match_identifier(text) or { return error('') }
  t2 := match_whitespace(t1)
  t3 := match_keyword(t2, '::') or { return error('') }
  t4 := match_whitespace(t3)
  t5 := match_identifier(t4) or { return error('') }
  t6 := match_whitespace(t5)
  t7 := match_char(t6, '(') or { return error('') }
  t8 := match_whitespace(t7)
  mut i := 0
  for i < t8.len {
    if t8[i] == `)` {
      break
    }
    i++
  }
  t9 := t8.substr(i, t8.len)
  return match_char(t9, ')')
}

fn match_regular_function_or_method(text string) ?string {
  mut text_new := text

  text_new = match_keyword_chain(text_new, ['static', 'meta', 'macro', 'cvar', 'inline'])

  text_new = value_or_default(text_new, match_keyword(text_new, 'extern'))
  text_new = match_whitespace(text_new)

  text_new = value_or_default(text_new, match_api(text_new))
  text_new = match_whitespace(text_new)

  // one more extern, because it can sometimes be after *_API
  text_new = value_or_default(text_new, match_keyword(text_new, 'extern'))
  text_new = match_whitespace(text_new)

  matched := match_type(text_new) or { return error('') }
  text_new = match_whitespace(matched)

  ident := match_identifier(text_new) or { return error('') }
  text_new = match_whitespace(ident)

  is_method := get_result(match_keyword(text_new, '::'))
  if is_method.ok {
    text_new = match_whitespace(is_method.value)
    matched_2 := match_identifier(text_new) or { return error('') }
    text_new = matched_2
  }

  params := match_char(text_new, '(') or { return error('') }
  text_new = match_whitespace(params)

  params_closed := match_until(text_new, ')') or { return error('') }
  if !is_end_of_line(params_closed) { return error('') }

  return params_closed
}

// TODO switch to bool-func
fn match_function(text string) ?string {
  matched := match_regular_function_or_method(text) or {
    // must be *structor
    matched_2 := match_structor(text) or {
      return error('')
    }
    return matched_2
  }
  return matched
}

fn count_chars(text string, c byte) int {
  mut count := 0
  for ct in text {
    if ct == c {
      count++
    }
  }
  return count
}

fn braces_balance(text string) int {
  return count_chars(text, `{`) - count_chars(text, `}`)
}

fn ansi_escape(codes []int) string {
  ansi := byte(27)
  mut str_codes := []string
  for code in codes {
    str_codes << code.str()
  }
  return tos(&ansi, 1) + '[' + str_codes.join(';') + 'm'
}

fn main() {
  if os.args.len < 3 {
    print_usage(os.args[0])
    println('Supplied args: $os.args')
    exit(1)
  }

  mut line_number := os.args[2].int() - 1
  if line_number < 0 {
    line_number = 0
  }

  lines := os.read_lines(os.args[1]) or {
    println('Failed reading ${os.args[1]}')
    exit(1)
  }

  if line_number >= lines.len {
    println('EOF')
    exit(1)
  }

  mut func_start := line_number
  mut func_end := line_number
  for func_start >= 0 {
    discard := match_function(lines[func_start]) or {
      func_start--
      continue
    }
    break
  }

  if func_start != -1 {
    mut current_braces_balance := braces_balance(lines[func_start])
    mut braces_line_number := func_start + 1

    mut finished := false
    for braces_line_number < lines.len && current_braces_balance == 0 {
      line := lines[braces_line_number]
      current_braces_balance += braces_balance(line)
      braces_line_number++

      if count_chars(line, `{`) == 1 && count_chars(line, `}`) == 1 && line.contains('{}') {
        func_end = braces_line_number
        finished = true
      }
    }

    for !finished && braces_line_number < lines.len && current_braces_balance != 0 {
      current_braces_balance += braces_balance(lines[braces_line_number])
      braces_line_number++
    }

    func_end = braces_line_number
  } else {
    func_start = line_number
    func_end = line_number + 1
  }

  if line_number < func_start || line_number >= func_end {
    func_start = line_number
    func_end = line_number + 1
  }

  for i := func_start; i < func_end; i++ {
    if i == line_number {
      print(ansi_escape([31, 1]))
    }
    print('${i + 1}: ')
    println(lines[i])
    if i == line_number {
      print(ansi_escape([0]))
    }
  }
}
